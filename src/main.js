import './styles/core/reset.css';
import './styles/core/base.css';
import './styles/core/loader.css';
import './styles/core/modal.css';

import './styles/pages/news.css';

import Vue from 'vue'
import App from './App.vue'

import store from './store/modules/news';

import VueObserveVisibility from 'vue-observe-visibility';
Vue.use(VueObserveVisibility);

Vue.config.productionTip = false

/** 
 * Using vue-js-modal library
*/
import vmodal from 'vue-js-modal';
Vue.use(vmodal);

/** 
 * Implementing Common Functions on VueJS Component Level
*/
import Common from '../utils/date'
Vue.mixin(Common)

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')