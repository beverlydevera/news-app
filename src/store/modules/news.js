// import dependency to handle HTTP request to our back end
import axios from 'axios'
import Vuex from 'vuex'
import Vue from 'vue'

//load Vuex
Vue.use(Vuex);

//to handle state
const state = {
    newsList: [],
    pageSize: 12,
    totalResults: 0,
    currentPage: 1,
    lastPage: 1,
    displayLoader: true,
    filter: {
        country: "ph",
        category: "",
        keyword: ""
    }
}

//to handle state
const getters = {
    // newsList: (state) => state.newsList
}

// constants for APIs
const NEWS_API = process.env.VUE_APP_NEWS_API;
const API_KEY = process.env.VUE_APP_NEWS_API_KEY;

//to handle actions
const actions = {
    async getNewsList({ state }) {
        let country = state.filter.country;
        let url = `${NEWS_API}top-headlines?page=${state.currentPage}&pageSize=${state.pageSize}&apiKey=${API_KEY}&country=${country}`;

        url = state.filter.category!="" ? `${url}&category=${state.filter.category}` : url;
        url = state.filter.keyword!="" ? `${url}&q=${state.filter.keyword}` : url;

        let response = await axios.get(url);
        return response;
    },
    async getAllNewsList({state,commit,dispatch}){
        state.currentPage = 1;

        let result = await dispatch('getNewsList');
        state.newsList = result.data.articles;
        state.totalResults = result.data.totalResults;
        state.lastPage = Math.ceil(state.totalResults/state.pageSize);

        commit("UPDATE_LOADER",false);
    },
    async handleScrollToBottom({state,commit,dispatch}){
        state.currentPage++;
        
        let result = await dispatch('getNewsList');
        state.newsList.push(...result.data.articles);

        commit("UPDATE_LOADER",false);
    },
}

//to handle mutations
const mutations = {
    UPDATE_LOADER(state,value){
        state.displayLoader = value;
    },
    UPDATE_COUNTRY(state, value) {
        state.filter.country = value;
    },
    UPDATE_CATEGORY(state, value) {
        state.filter.category = value;
    },
    UPDATE_KEYWORD(state, value) {
        state.filter.keyword = value;
    }
}

//export store module
export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations
})