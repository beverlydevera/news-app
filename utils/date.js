export default {
    methods: {
        formatPublishDate(date) {
            let formatDate = "";
            if(date){
                let d = new Date(date);
                let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
                let mo = new Intl.DateTimeFormat('en', { month: 'long' }).format(d);
                let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
    
                formatDate = `${mo} ${da}, ${ye}`;
            }
            return formatDate;
        }
    }
}